package com.pizzeria.test;

import com.pizzeria.domain.User;
import com.pizzeria.services.UserServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collection;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
@ActiveProfiles("test")
public class UserServiceTests {
    @Rule
    public ExpectedException exception = ExpectedException.none();
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected WebApplicationContext wac;
    private MockMvc mockMvc;
    @Autowired
    private ApplicationContext applicationContext;
    private UserServiceImpl userService;
    private User.Builder adminBuilder;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
        userService = applicationContext.getBean(UserServiceImpl.class);
        adminBuilder = User.builder()
                .setEmail("admin@test.it")
                .setUsername("admin")
                .setPassword("password")
                .setRole(User.Role.ADMIN);
    }

    @Test
    public void getUserTest() throws Exception {
        User user = userService.getUser("0");
        assertNotNull(user);
        user = userService.getUser("10");
        assertNull(user);
    }

    @Test
    public void insertTest() throws Exception {
        User.Builder userBuilder = User.builder()
                .setEmail("insertTest@test.it")
                .setUsername("insertTest")
                .setPassword("password")
                .setRole(User.Role.NORMAL);

        User user = userBuilder.build();
        User newUser = userService.insertData(user, null);
        assertNotNull(newUser);
        assertNotNull(newUser.getId());
        user = userBuilder.setUsername(null).build();
        exception.expect(Exception.class);
        // Must be throw a Exception
        userService.insertData(user, null);
    }

    @Test
    public void insertUserOrEmailAlreadyExistsTest() throws Exception {
        User.Builder userBuilder = User.builder()
                .setEmail("insertUserOrEmailAlreadyExistsTest@test.it")
                .setUsername("insertUserOrEmailAlreadyExistsTest")
                .setPassword("password")
                .setRole(User.Role.NORMAL);

        User user = userBuilder.build();
        userService.insertData(user, null);
        exception.expect(Exception.class);
        // Must be throw a Exception
        userService.insertData(user, null);
    }

    @Test
    public void insertSecureAnonymousUserCannotCreateAAdminUserTest() throws Exception {
        User.Builder userBuilder = User.builder()
                .setEmail("test@test.it")
                .setUsername("test")
                .setPassword("password")
                .setRole(User.Role.ADMIN);

        User user = userBuilder.build();
        exception.expect(Exception.class);
        userService.insertData(user, null);

    }

    @Test
    public void insertSecureNormalUserCannotCreateAAdminUserTest() throws Exception {
        User.Builder userBuilder = User.builder()
                .setEmail("insertSecureNormalUserCannotCreateAAdminUserTest@test.it")
                .setUsername("insertSecureNormalUserCannotCreateAAdminUserTest")
                .setPassword("password")
                .setRole(User.Role.ADMIN);

        User user = userBuilder.build();
        exception.expect(Exception.class);

        userService.insertData(user, new User.Builder()
                .setFromUser(adminBuilder.build())
                .setRole(User.Role.NORMAL).build());

    }


    @Test
    public void deleteTest() throws Exception {
        User user = User.builder()
                .setEmail("deleteTest@test.it")
                .setUsername("deleteTest")
                .setPassword("password")
                .setRole(User.Role.NORMAL).build();
        User newUser = userService.insertData(user, null);
        assertNotNull(newUser);
        userService.deleteData(newUser.getId().toString());
        user = userService.getUser(newUser.getId().toString());
        assertNull(user);
    }

    @Test
    public void updateTest() throws Exception {
        User.Builder userBuilder = User.builder()
                .setEmail("updateTest@test.it")
                .setUsername("updateTest")
                .setPassword("password")
                .setRole(User.Role.NORMAL);
        User user = userService.insertData(userBuilder.build(), null);
        assertNotNull(user);
        String newEmail = "newTestEmail@email.com";
        String newPassword = "newPassword";

        userService.updateData(userBuilder
                .setId(user.getId())
                .setEmail(newEmail)
                .setPassword(newPassword).build());
        user = userService.login(newEmail, newPassword);
        assertNotNull(user);
        assertEquals(newEmail, user.getEmail());
        user = userService.login(newEmail, newPassword + "not valid password");
        assertNull(user);
    }

    @Test
    public void getUserListTest() throws Exception {
        Collection<User> users = userService.getUserList();
        assertNotNull(users);
    }

}
