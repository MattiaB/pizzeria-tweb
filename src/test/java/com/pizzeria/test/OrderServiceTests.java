package com.pizzeria.test;

import com.pizzeria.domain.Order;
import com.pizzeria.domain.Pizza;
import com.pizzeria.domain.SelectedPizza;
import com.pizzeria.domain.User;
import com.pizzeria.services.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
@ActiveProfiles("test")
public class OrderServiceTests {
    private MockMvc mockMvc;
    @Rule
    public ExpectedException exception = ExpectedException.none();
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    private ApplicationContext applicationContext;

    private OrderService orderService;
    private UserService userService;

    private Date date = new Date();

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
        orderService = applicationContext.getBean(OrderService.class);
        userService = applicationContext.getBean(UserServiceImpl.class);
    }

    @Test
    public void getOrderTest() throws Exception {
        Order order = orderService.getOrder("0");
        assertNotNull(order);
        order = orderService.getOrder("10");
        assertNull(order);
    }

    @Test
    public void insertTest() throws Exception {
        Order.Builder orderBuilder = Order.builder();
        User user = userService.getUser("0");
        Order order = orderBuilder
                .setUser(user).setDeliveryDate(date).build();
        Order newOrder = orderService.insertData(order);
        assertNotNull(newOrder);
        assertNotNull(newOrder.getId());
        order = orderBuilder.setUser(null).build();
        exception.expect(DataIntegrityViolationException.class);
        // Must be throw a DataIntegrityViolationException
        newOrder = orderService.insertData(order);
    }

    @Test
    public void deleteTest() throws Exception {
        User user = userService.getUser("1");
        Order order = Order.builder()
                .setUser(user).setDeliveryDate(date).build();
        Order newOrder = orderService.insertData(order);
        assertNotNull(newOrder);
//        orderService.deleteData(newOrder.getId().toString(), user);
//        order = orderService.getOrder(newOrder.getId().toString());
//        assertNull(order);
    }

    @Test
    public void updateTest() throws Exception {
        User user = userService.getUser("0");
        Order order = Order.builder()
                .setUser(user).setDeliveryDate(date).build();
        order = orderService.insertData(order);
        assertNotNull(order);
        Date newData = new Date();
        User newUser = userService.getUser("1");

        orderService.updateData(
                Order.builder().setId(order.getId())
                        .setUser(newUser).setDeliveryDate(newData).build());
//        order = orderService.getOrder(order.getId().toString());
//        assertNotNull(order);
//        assertEquals(newData, order.getDeliveryDate());
//        assertEquals(newUser.getId(), order.getUser().getId());
    }


    @Test
    public void getUserListTest() throws Exception {
        Collection<Order> pizzas = orderService.getOrderList();
        assertNotNull(pizzas);
    }

    @Test
    public void insertSelectedPizzasTest() throws Exception {
        PizzaService pizzaService = applicationContext.getBean(PizzaServiceImpl.class);
        Pizza pizza = pizzaService.getPizza("0");
        Order order = orderService.insertData(Order.builder()
                .setUser(userService.getUser("0")).setDeliveryDate(date).build());
        SelectedPizza selectedPizza = SelectedPizza.builder()
                .setPizza(pizza).setQuantity(10).setOrder(order).build();
        Collection<SelectedPizza> selectedPizzaCollection = new ArrayList<SelectedPizza>();
        selectedPizzaCollection.add(selectedPizza);
        orderService.insertSelectedPizzas(order, selectedPizzaCollection);
    }
}
