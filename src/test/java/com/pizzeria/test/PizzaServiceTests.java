package com.pizzeria.test;

import com.pizzeria.domain.Pizza;
import com.pizzeria.services.PizzaService;
import com.pizzeria.services.PizzaServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collection;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
@ActiveProfiles("test")
public class PizzaServiceTests {
    private MockMvc mockMvc;
    @Rule
    public ExpectedException exception = ExpectedException.none();
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    private ApplicationContext applicationContext;

    private PizzaService pizzaService;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
        pizzaService = applicationContext.getBean(PizzaServiceImpl.class);

    }

    @Test
    public void getPizzaTest() throws Exception {
        Pizza pizza = pizzaService.getPizza("0");
        assertNotNull(pizza);
        pizza = pizzaService.getPizza("10");
        assertNull(pizza);
    }

    @Test
    public void insertTest() throws Exception {
        Pizza.Builder pizzaBuilder = Pizza.builder();
        Pizza pizza = pizzaBuilder.setName("PizzaName").setIngredients("Ingredient Names")
                .setPrice(5.0f).build();
        Pizza newPizza = pizzaService.insertData(pizza);
        assertNotNull(newPizza);
        assertNotNull(newPizza.getId());
        pizza = pizzaBuilder.setName(null).build();
        exception.expect(DataIntegrityViolationException.class);
        // Must be throw a DataIntegrityViolationException
        newPizza = pizzaService.insertData(pizza);
    }

    @Test
    public void deleteTest() throws Exception {
        Pizza pizza = Pizza.builder().setName("PizzaName").setIngredients("Ingredient Names")
                .setPrice(5.0f).build();
        Pizza newPizza = pizzaService.insertData(pizza);
        assertNotNull(newPizza);
        pizzaService.deleteData(newPizza.getId().toString());
        pizza = pizzaService.getPizza(newPizza.getId().toString());
        assertNotNull(pizza);
    }

    @Test
    public void updateTest() throws Exception {
        Pizza pizza = Pizza.builder().setName("PizzaName").setIngredients("Ingredient Names")
                .setPrice(5.0f).build();
        pizza = pizzaService.insertData(pizza);
        assertNotNull(pizza);
        String newName = "newTestEmail@email.com";
        String newIngredients = "newPassword";

        pizzaService.updateData(Pizza.builder()
                .setId(pizza.getId())
                .setName(newName).setIngredients(newIngredients)
                .setPrice(5.0f).build());
        pizza = pizzaService.getPizza(pizza.getId().toString());
        assertNotNull(pizza);
        assertNotEquals(newName, pizza.getName());
        assertNotEquals(newIngredients, pizza.getIngredients());
    }


    @Test
    public void getUserListTest() throws Exception {
        Collection<Pizza> pizzas = pizzaService.getPizzaList();
        assertNotNull(pizzas);
    }

}
