package com.pizzeria.test;


import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
@ActiveProfiles("test")
public class PizzaControllerTests extends ControllerTests {

    @Test
    public void newPizza() throws Exception {
        mockMvc.perform(get("/pizza/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("pizzaCreateForm"))
                .andExpect(model().attributeExists("pizzaForm"))
                .andExpect(model().attribute("actionType", CoreMatchers.equalTo("create")));
    }
}
