package com.pizzeria.dao;


import com.pizzeria.domain.Pizza;

import java.util.Collection;

public interface PizzaDao {

    public Pizza insertData(Pizza pizza);

    public Collection<Pizza> getPizzaList();

    public void updateData(Pizza pizza);

    public void deleteData(String id);

    public Pizza getPizza(String id);
}
