package com.pizzeria.dao;


import com.pizzeria.domain.Order;
import com.pizzeria.domain.SelectedPizza;

import java.util.Collection;

public interface OrderDao {

    public Order insertData(Order order);

    public Collection<Order> getOrderList();

    public Collection<Order> getOrderListByUserId(Number userId);

    public Collection<SelectedPizza> getOrderedPizzas(Order order);

    public void updateData(Order order);

    public void deleteData(String id);

    public Order getOrder(String id);

    int[] insertSelectedPizzas(Order order, Collection<SelectedPizza> selectedPizzas);
}
