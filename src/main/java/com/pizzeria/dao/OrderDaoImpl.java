package com.pizzeria.dao;


import com.pizzeria.domain.Order;
import com.pizzeria.domain.SelectedPizza;
import com.pizzeria.jdbc.OrderExtractor;
import com.pizzeria.jdbc.SelectedPizzaRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.*;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrderDaoImpl implements OrderDao {

    public final String QUERY_COMPLETE_ORDER = "SELECT " +
            "orders.id AS orders_id, orders.delivery_date AS orders_delivery_date, " +
            "orders.user_id AS orders_user_id, orders.complete AS orders_complete, " +
            "orders.modifiable AS orders_modifiable, " +
            "ord_pizzas.id AS ord_pizzas_id, ord_pizzas.quantity AS ord_pizzas_quantity, " +
            "pizzas.id AS pizzas_id, pizzas.name AS pizzas_name, " +
            "pizzas.ingredients AS pizzas_ingredients, pizzas.price AS pizzas_price, " +
            "users.username AS users_username " +
            "FROM orders " +
            "JOIN order_pizzas ord_pizzas ON orders.id = ord_pizzas.order_id " +
            "JOIN pizzas ON ord_pizzas.pizza_id = pizzas.id " +
            "JOIN users ON orders.user_id = users.id ";

    public final String QUERY_COMPLETE_ORDER_ORDERBY = " ORDER BY orders.delivery_date DESC";

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert insertOrder;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.insertOrder = new SimpleJdbcInsert(dataSource)
                .withTableName("orders").usingGeneratedKeyColumns("id");
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Order insertData(Order order) {
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(order);
        Number newId = insertOrder.executeAndReturnKey(parameters);
        return Order.builder().setFromOrder(order).setId(newId).build();
    }

    @Override
    public int[] insertSelectedPizzas(Order order, Collection<SelectedPizza> selectedPizzas) {
        final String sql = "INSERT INTO order_pizzas (pizza_id, order_id, quantity) " +
                "VALUES (:pizza_id, :order_id, :quantity)";
        SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(selectedPizzas.toArray());
        return namedParameterJdbcTemplate.batchUpdate(sql, batch);
    }

    @Override
    public Collection<Order> getOrderList() {
        return getOrderListOrdered(jdbcTemplate.query(
                QUERY_COMPLETE_ORDER + QUERY_COMPLETE_ORDER_ORDERBY,
                new OrderExtractor()));
    }

    @Override
    public Collection<Order> getOrderListByUserId(Number userId) {
        final String sql = QUERY_COMPLETE_ORDER + " WHERE orders.user_id = ?" + QUERY_COMPLETE_ORDER_ORDERBY;
        return getOrderListOrdered(jdbcTemplate.query(sql, new OrderExtractor(), userId));
    }

    @Override
    public List<SelectedPizza> getOrderedPizzas(Order order) {
        final String sql = "SELECT * FROM order_pizzas WHERE order_id = ?";
        return jdbcTemplate.query(sql, new SelectedPizzaRowMapper(), order.getId());
    }

    @Override
    public void updateData(Order order) {
        final String sql = "UPDATE orders " +
                "SET user_id = :user_id, delivery_date = :delivery_date, " +
                "complete = :complete, modifiable = :modifiable WHERE id = :id";

        SqlParameterSource namedParameters =
                new MapSqlParameterSource()
                        .addValue("user_id", order.getUserId())
                        .addValue("delivery_date", order.getDeliveryDate())
                        .addValue("id", order.getId())
                        .addValue("complete", order.getComplete())
                        .addValue("modifiable", order.getModifiable());
        namedParameterJdbcTemplate.update(sql, namedParameters);
    }

    @Override
    public void deleteData(String id) {
        final String sql = "DELETE FROM orders WHERE id= ?";
        jdbcTemplate.update(sql, Integer.valueOf(id));
        final String OrderPizzasSql = "DELETE FROM order_pizzas WHERE order_id = ?";
        jdbcTemplate.update(OrderPizzasSql, Integer.valueOf(id));
    }

    @Override
    public Order getOrder(String id) {
        final String sql = QUERY_COMPLETE_ORDER + " WHERE orders.id = ?";
        Collection<Order> orders = jdbcTemplate.query(sql,
                new OrderExtractor(), Integer.valueOf(id));
        if (orders.isEmpty()) {
            return null;
        } else {
            return orders.iterator().next();
        }
    }

    private List<Order> getOrderListOrdered(List<Order> orderList) {
        Collections.sort(orderList, new Comparator<Order>() {

            @Override
            public int compare(Order o1, Order o2) {
                return o2.getDeliveryDate().compareTo(o1.getDeliveryDate());
            }
        });
        return orderList;
    }
}
