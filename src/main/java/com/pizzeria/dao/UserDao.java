package com.pizzeria.dao;

import com.pizzeria.domain.User;

import java.util.Collection;


public interface UserDao {
    public User insertData(User user) throws Exception;

    public Collection<User> getUserList();

    public void updateData(User user);

    public void deleteData(String id);

    public User getUser(String id);

    public User getUserByEmailOrUsernameAndPassword(String emailOrUsername, String password);
}

