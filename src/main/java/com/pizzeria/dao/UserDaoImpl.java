package com.pizzeria.dao;

import com.pizzeria.domain.User;
import com.pizzeria.jdbc.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert insertUser;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.insertUser = new SimpleJdbcInsert(dataSource)
                .withTableName("users").usingGeneratedKeyColumns("id");
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public User insertData(User user) throws Exception {
        if (existUserWithThisField(user.getEmail(), "email")) {
            throw new Exception("Email already exists");
        }
        if (existUserWithThisField(user.getUsername(), "username")) {
            throw new Exception("Username already exists");
        }
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(user);
        Number newId = insertUser.executeAndReturnKey(parameters);
        return User.builder().setFromUser(user).setId(newId).build();
    }

    @Override
    public List<User> getUserList() {
        final String sql = "SELECT * FROM users";
        return jdbcTemplate.query(sql, new UserRowMapper());
    }

    @Override
    public void updateData(User user) {
        final String sql = "UPDATE users " +
                "SET email = :email,username = :username, password = :password, role = :role WHERE id = :id";

        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(user);
        namedParameterJdbcTemplate.update(sql, namedParameters);

    }

    @Override
    public void deleteData(String id) {
        final String sql = "DELETE FROM users WHERE id= ?";
        jdbcTemplate.update(sql, Integer.valueOf(id));
    }

    @Override
    public User getUser(String id) {
        final String sql = "SELECT * FROM users WHERE id= ?";
        List<User> users = jdbcTemplate.query(sql, new UserRowMapper(), Integer.valueOf(id));
        return getFirst(users);
    }

    @Override
    public User getUserByEmailOrUsernameAndPassword(String emailOrUsername, String password) {
        final String sql = "SELECT id, username, email, role FROM users WHERE " +
                "(email = ? OR username = ?) AND password = ?";

        List<User> users = jdbcTemplate.query(sql, new UserRowMapper(),
                emailOrUsername, emailOrUsername, password);
        return getFirst(users);
    }

    private boolean existUserWithThisField(String email, String field) {
        final String sql = "SELECT COUNT(*) FROM users WHERE " + field + " = ?";
        Integer countResult = jdbcTemplate.queryForObject(sql, new Object[]{email}, Integer.class);
        return countResult > 0;

    }

    private User getFirst(List<User> users) {
        if (users.isEmpty()) {
            return null;
        } else {
            return users.get(0);
        }
    }
}
