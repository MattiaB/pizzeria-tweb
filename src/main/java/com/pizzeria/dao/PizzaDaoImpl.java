package com.pizzeria.dao;


import com.pizzeria.domain.Pizza;
import com.pizzeria.jdbc.PizzaRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class PizzaDaoImpl implements PizzaDao {

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert insertPizza;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.insertPizza = new SimpleJdbcInsert(dataSource)
                .withTableName("pizzas").usingGeneratedKeyColumns("id");
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Pizza insertData(Pizza pizza) {
        pizza = Pizza.builder().setFromPizza(pizza).setDeleted(false).build();
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(pizza);
        Number newId = insertPizza.executeAndReturnKey(parameters);
        return Pizza.builder().setFromPizza(pizza).setId(newId).build();
    }

    @Override
    public List<Pizza> getPizzaList() {
        final String sql = "SELECT * FROM pizzas WHERE deleted = FALSE";
        return jdbcTemplate.query(sql, new PizzaRowMapper());
    }

    @Override
    public void updateData(Pizza pizza) {
        insertData(pizza);
        deleteData(pizza.getId().toString());

    }

    @Override
    public void deleteData(String id) {
        Pizza foundPizza = getPizza(id);
        Pizza deletedPizza = Pizza.builder().setFromPizza(foundPizza).setDeleted(true).build();
        onlyUpdate(deletedPizza);
    }

    @Override
    public Pizza getPizza(String id) {
        final String sql = "SELECT * FROM pizzas WHERE id= ?";
        List<Pizza> pizzas = jdbcTemplate.query(sql, new PizzaRowMapper(), Integer.valueOf(id));
        if (pizzas.isEmpty()) {
            return null;
        } else {
            return pizzas.get(0);
        }
    }


    private void onlyUpdate(Pizza pizza) {
        final String sql = "UPDATE pizzas SET name = :name, ingredients = :ingredients, " +
                "price = :price, deleted = :deleted WHERE id = :id";
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(pizza);
        namedParameterJdbcTemplate.update(sql, namedParameters);
    }
}
