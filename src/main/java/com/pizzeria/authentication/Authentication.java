package com.pizzeria.authentication;


import com.pizzeria.domain.User;

public interface Authentication {
    public boolean isAdminUser();

    public boolean isNormalUser();

    public boolean isLogged();

    public String isAdminUserWithRedirectToLogin();

    public String isNormalUserWithRedirectToLogin();

    public String isLoggedWithRedirectToLogin();

    public User getUserFromSession();

    public String getRedirectToLogin();


}
