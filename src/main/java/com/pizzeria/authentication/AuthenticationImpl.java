package com.pizzeria.authentication;


import com.pizzeria.domain.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;

public class AuthenticationImpl implements Authentication {
    @Autowired
    private HttpSession session;
    private final String LOGIN_URL = "redirect:/login";

    @Override
    public boolean isAdminUser() {
        User user = getUserFromSession();
        return user != null && user.isAdmin();
    }

    @Override
    public boolean isNormalUser() {
        User user = getUserFromSession();
        return user != null && user.isNormal();
    }

    @Override
    public boolean isLogged() {
        return getUserFromSession() != null;
    }

    @Override
    public String isAdminUserWithRedirectToLogin() {
        return getLoginUrlIfFalse(isAdminUser());
    }

    @Override
    public String isNormalUserWithRedirectToLogin() {
        return getLoginUrlIfFalse(isNormalUser());
    }

    @Override
    public String isLoggedWithRedirectToLogin() {
        return getLoginUrlIfFalse(isLogged());
    }

    @Override
    public User getUserFromSession() {
        return (User) session.getAttribute("user");
    }

    @Override
    public String getRedirectToLogin() {
        return LOGIN_URL;
    }

    private String getLoginUrlIfFalse(boolean condition) {
        if (!condition) {
            return LOGIN_URL;
        }
        return null;
    }
}
