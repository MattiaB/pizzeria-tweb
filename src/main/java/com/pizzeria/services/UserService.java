package com.pizzeria.services;

import com.pizzeria.domain.User;

import java.util.Collection;

public interface UserService {
    public User insertData(User user, User whoCreateTheUser) throws Exception;

    public Collection<User> getUserList();

    public void deleteData(String id);

    public User getUser(String id);

    public void updateData(User user);

    public User login(String emailOrUsername, String password);
}
