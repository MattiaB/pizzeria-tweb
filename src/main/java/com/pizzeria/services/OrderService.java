package com.pizzeria.services;


import com.pizzeria.domain.Order;
import com.pizzeria.domain.SelectedPizza;
import com.pizzeria.domain.User;

import java.util.Collection;
import java.util.Map;

public interface OrderService {
    public Order insertData(Order order);

    public Collection<Order> getOrderList();

    public Collection<Order> getOrderListByUser(User user);

    public void deleteData(String id, User user) throws Exception;

    public Order getOrder(String id);

    public void updateData(Order order);

    public void insertSelectedPizzas(Order order, Collection<SelectedPizza> selectedPizzas);

    Collection<SelectedPizza> getOrderedPizzas(Order order);

    public void setComplete(String id, User user) throws Exception;

    public Map getOrderMap(String id);

    public void setUnchangeable(String idString, User user) throws Exception;

    public void setChangeable(String idString, User user) throws Exception;

    public void setUncompleted(String id, User user) throws Exception;

    public Order insertOrUpdateOrderByTempOrderMap(Order newOrder, Map tempOrder, User user) throws Exception;
}
