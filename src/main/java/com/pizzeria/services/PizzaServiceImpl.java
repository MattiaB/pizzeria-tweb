package com.pizzeria.services;


import com.pizzeria.dao.PizzaDao;
import com.pizzeria.domain.Pizza;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

public class PizzaServiceImpl implements PizzaService {
    @Autowired
    PizzaDao pizzadao;

    @Override
    public Pizza insertData(Pizza pizza) {
        return pizzadao.insertData(pizza);
    }

    @Override
    public Collection<Pizza> getPizzaList() {
        return pizzadao.getPizzaList();
    }

    @Override
    public void deleteData(String id) {
        pizzadao.deleteData(id);

    }

    @Override
    public Pizza getPizza(String id) {
        return pizzadao.getPizza(id);
    }

    @Override
    public void updateData(Pizza pizza) {
        pizzadao.updateData(pizza);

    }
}
