package com.pizzeria.services;


import com.pizzeria.domain.Pizza;

import java.util.Collection;

public interface PizzaService {
    public Pizza insertData(Pizza pizza);

    public Collection<Pizza> getPizzaList();

    public void deleteData(String id);

    public Pizza getPizza(String id);

    public void updateData(Pizza pizza);
}
