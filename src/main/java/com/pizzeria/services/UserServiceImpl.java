package com.pizzeria.services;

import com.pizzeria.dao.UserDao;
import com.pizzeria.domain.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

public class UserServiceImpl implements UserService {
    @Autowired
    UserDao userdao;

    @Override
    public User insertData(User user, User whoCreateTheUser) throws Exception {
        User newUser = null;
        if (whoCreateTheUser == null) {
            if (user.getEnumRole() == User.Role.ADMIN) {
                throw new Exception("You are not allowed to create a admin user");
            }
            newUser = User.builder().setFromUser(user).setRole(User.Role.NORMAL).build();
        } else if (whoCreateTheUser.getEnumRole() == User.Role.NORMAL) {
            throw new Exception("You are not allowed to create a user");
        } else {
            newUser = user;
        }
        return userdao.insertData(newUser);
    }

    @Override
    public Collection<User> getUserList() {
        return userdao.getUserList();
    }

    @Override
    public void deleteData(String id) {
        userdao.deleteData(id);

    }

    @Override
    public User getUser(String id) {
        return userdao.getUser(id);
    }

    @Override
    public void updateData(User user) {
        userdao.updateData(user);

    }

    @Override
    public User login(String emailOrUsername, String password) {
        return userdao.getUserByEmailOrUsernameAndPassword(emailOrUsername, password);
    }
}
