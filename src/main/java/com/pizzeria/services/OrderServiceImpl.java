package com.pizzeria.services;


import com.pizzeria.dao.OrderDao;
import com.pizzeria.domain.Order;
import com.pizzeria.domain.Pizza;
import com.pizzeria.domain.SelectedPizza;
import com.pizzeria.domain.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderDao orderdao;

    @Override
    public Order insertData(Order order) {
        return orderdao.insertData(order);
    }

    @Override
    public Collection<Order> getOrderList() {
        return orderdao.getOrderList();
    }

    @Override
    public Collection<Order> getOrderListByUser(User user) {
        if (user.isAdmin()) {
            return orderdao.getOrderList();
        } else {
            return orderdao.getOrderListByUserId(user.getId());
        }
    }

    @Override
    public Collection<SelectedPizza> getOrderedPizzas(Order order) {
        return orderdao.getOrderedPizzas(order);
    }

    @Override
    public void setComplete(String id, User user) throws Exception {
        Order order = checkOrderPrivilege(id, user);
        Order newOrder = Order.builder().setFromOrder(order).setComplete(true).build();
        orderdao.updateData(newOrder);
    }

    @Override
    public void setUncompleted(String id, User user) throws Exception {
        Order order = checkOrderPrivilege(id, user);
        Order newOrder = Order.builder().setFromOrder(order).setComplete(false).build();
        orderdao.updateData(newOrder);
    }

    @Override
    public void deleteData(String id, User user) throws Exception {
        Order order = checkOrderPrivilege(id, user);
        isModify(order);
        orderdao.deleteData(id);

    }

    @Override
    public Order getOrder(String id) {
        return orderdao.getOrder(id);
    }

    @Override
    public void updateData(Order order) {
        orderdao.updateData(order);

    }

    @Override
    public void insertSelectedPizzas(Order order, Collection<SelectedPizza> selectedPizzas) {
        orderdao.insertSelectedPizzas(order, selectedPizzas);
    }

    @Override
    public Map getOrderMap(String id) {
        Order order = orderdao.getOrder(id);
        Map<String, Object> orderMap = new HashMap<>();
        orderMap.put("id", order.getId());
        orderMap.put("date", order.getDeliveryDate());
        orderMap.put("complete", order.getComplete());
        orderMap.put("modifiable", order.getModifiable());
        orderMap.put("total_price", order.getTotalPrice());
        orderMap.put("total_quantity", order.getTotalQuantity());
        Collection<Map> selectedPizzas = new ArrayList<>(order.getSelectedPizzas().size());

        for (SelectedPizza selectedPizza : order.getSelectedPizzas()) {
            Map<String, Object> selectedPizzaMap = new HashMap<>();
            selectedPizzaMap.put("id", selectedPizza.getPizza_id());
            selectedPizzaMap.put("ingredients", selectedPizza.getPizza().getIngredients());
            selectedPizzaMap.put("name", selectedPizza.getPizza().getName());
            selectedPizzaMap.put("pizza_price", selectedPizza.getPizza().getPrice());
            selectedPizzaMap.put("quantity", selectedPizza.getQuantity());
            selectedPizzaMap.put("total_price", selectedPizza.getTotalPrice());
            selectedPizzas.add(selectedPizzaMap);

        }
        orderMap.put("selectedPizzas", selectedPizzas);
        return orderMap;
    }

    @Override
    public void setUnchangeable(String id, User user) throws Exception {
        Order order = checkOrderPrivilege(id, user);
        isModify(order);
        Order newOrder = Order.builder().setFromOrder(order).setModifiable(false).build();
        orderdao.updateData(newOrder);
    }

    @Override
    public void setChangeable(String id, User user) throws Exception {
        Order order = orderdao.getOrder(id);
        if (!(user.isAdmin())) {
            throw new Exception("You don't have the privilege to do this");
        }
        Order newOrder = Order.builder().setFromOrder(order).setModifiable(true).build();
        orderdao.updateData(newOrder);
    }

    @Override
    public Order insertOrUpdateOrderByTempOrderMap(Order newOrder,
                                                   Map tempOrder,
                                                   User user) throws Exception {
        Order order = insertData(newOrder);

        Collection<Map> selectedPizzas = (Collection) tempOrder.get("selectedPizzas");
        Collection<SelectedPizza> selectedPizzaCollections = new LinkedList<SelectedPizza>();
        for (Map pizza : selectedPizzas) {
            selectedPizzaCollections.add(
                    SelectedPizza.builder()
                            .setQuantity((Number) pizza.get("quantity"))
                            .setPizza(Pizza.builder()
                                    .setId((Number) pizza.get("id"))
                                    .build())
                            .setOrder(order)
                            .build()
            );
        }
        insertSelectedPizzas(order, selectedPizzaCollections);

        if (tempOrder.containsKey("id")) {
            deleteData(tempOrder.get("id").toString(), user);
        }
        return order;
    }

    private Order checkOrderPrivilege(String id, User user) throws Exception {
        Order order = orderdao.getOrder(id);
        if (!(user.isAdmin() || order.getUserId().equals(user.getId()))) {
            throw new Exception("You don't have the privilege to do this");
        }
        return order;
    }

    private void isModify(Order order) throws Exception {
        if (!order.getModifiable()) {
            throw new Exception("You cannot modify the order because is unchangeable");
        }
    }
}
