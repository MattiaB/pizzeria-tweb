package com.pizzeria.controller.form;

import org.hibernate.validator.constraints.NotEmpty;

public class UserLoginFormBean {
    @NotEmpty
    private String emailOrUsername;
    @NotEmpty(message = "Password must not be blank.")
    private String password;

    public String getEmailOrUsername() {
        return emailOrUsername;
    }

    public void setEmailOrUsername(String emailOrUsername) {
        this.emailOrUsername = emailOrUsername;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
