package com.pizzeria.controller.form;


import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderFormBean {
    final static private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    final static private SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm");
    final static private SimpleDateFormat simpleCompleteDateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");

    @NotEmpty
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String date;

    @NotEmpty
    @DateTimeFormat(pattern = "HH:mm")
    private String time;

    private Date completeDate;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean validate(BindingResult result) {
        try {
            simpleDateFormat.parse(getDate());
        } catch (ParseException parseException) {
            result.addError(new FieldError("orderForm", "date", "The date format is wrong. Choose a date in this format dd/MM/yyyy ex. 20/10/2013"));
        }
        try {
            simpleTimeFormat.parse(getTime());
        } catch (ParseException parseException) {
            result.addError(new FieldError("orderForm", "time", "The time format is wrong. Choose a time in this format hh:mm ex. 13:45"));
        }
        if (!result.hasErrors()) {
            try {
                setCompleteDate(simpleCompleteDateFormat.parse(getDate() + " - " + getTime()));
                if (!getCompleteDate().after(new Date())) {
                    result.addError(new FieldError("orderForm", "date", "The date and time of delivery must be after now"));
                }
            } catch (ParseException parseException) {
                setCompleteDate(null);
            }
        }
        return result.hasErrors();
    }

    public Date getCompleteDate() {
        return completeDate;
    }

    public String getCompleteDateString() {
        return simpleCompleteDateFormat.format(getCompleteDate());
    }

    public void setCompleteDate(Date completeDate) {
        setDate(simpleDateFormat.format(completeDate));
        setTime(simpleTimeFormat.format(completeDate));
        this.completeDate = completeDate;
    }
}
