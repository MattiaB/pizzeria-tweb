package com.pizzeria.controller.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.Size;

@ScriptAssert(lang = "javascript",
        script = "_this.confirmPassword.equals(_this.password)",
        message = "The password and confirm password must be the same.")
public class UserSingUpFormBean {
    @NotEmpty
    @Email
    private String email;
    @NotEmpty
    private String username;

    @NotEmpty(message = "Password must not be blank.")
    @Size(min = 5, max = 40, message = "Password must between 5 to 40 Characters.")
    private String password;

    @NotEmpty(message = "Confirm Password must not be blank.")
    @Size(min = 5, max = 40, message = "Password must between 5 to 40 Characters.")
    private String confirmPassword;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
