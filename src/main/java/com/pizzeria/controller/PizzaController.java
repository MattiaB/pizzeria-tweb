package com.pizzeria.controller;


import com.pizzeria.authentication.Authentication;
import com.pizzeria.controller.form.PizzaFormBean;
import com.pizzeria.domain.Pizza;
import com.pizzeria.services.PizzaService;
import com.pizzeria.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/pizza")
public class PizzaController {
    @Autowired
    UserService userService;
    @Autowired
    PizzaService pizzaService;
    @Autowired
    Authentication authentication;

    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String newPizza(ModelMap model) {
        if (!authentication.isAdminUser()) {
            return authentication.getRedirectToLogin();
        }
        model.addAttribute("pizzaForm", new PizzaFormBean());
        model.addAttribute("actionType", "create");
        return "pizzaCreateForm";
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public String create(@ModelAttribute("pizzaForm") @Valid PizzaFormBean pizzaForm,
                         BindingResult result,
                         RedirectAttributes redirectAttrs) {
        if (!authentication.isAdminUser()) {
            return authentication.getRedirectToLogin();
        }
        Pizza pizza = Pizza.builder()
                .setIngredients(pizzaForm.getIngredients())
                .setName(pizzaForm.getName())
                .setPrice(pizzaForm.getPrice()).build();
        if (result.hasErrors()) {
            return "pizzaCreateForm";
        }
        pizzaService.insertData(pizza);
        redirectAttrs.addFlashAttribute("messageSuccess", "The "
                + pizza.getName()
                + " pizza created with success ");
        return "redirect:/pizza/list";
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String showList(ModelMap model) {
        if (!authentication.isAdminUser()) {
            return authentication.getRedirectToLogin();
        }
        Collection<Pizza> pizzas = pizzaService.getPizzaList();
        model.put("pizzas", pizzas);
        return "pizzasList";
    }

    @RequestMapping(value = "list.json", method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<Pizza> showListInJson() {
        return pizzaService.getPizzaList();
    }

    @RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
    public String edit(ModelMap model, @PathVariable("id") String id) {
        if (!authentication.isAdminUser()) {
            return authentication.getRedirectToLogin();
        }
        Pizza pizza = pizzaService.getPizza(id);
        model.put("pizzaForm", pizza);
        model.put("actionType", pizza.getId() + "/update");
        return "pizzaCreateForm";
    }

    @RequestMapping(value = "{id}/update", method = RequestMethod.POST)
    public String update(PizzaFormBean pizzaForm,
                         @PathVariable("id") String id,
                         RedirectAttributes redirectAttrs) {
        if (!authentication.isAdminUser()) {
            return authentication.getRedirectToLogin();
        }
        Pizza pizza = Pizza.builder()
                .setId(Integer.parseInt(id))
                .setIngredients(pizzaForm.getIngredients())
                .setName(pizzaForm.getName())
                .setPrice(pizzaForm.getPrice()).build();
        pizzaService.updateData(pizza);
        redirectAttrs.addFlashAttribute("messageSuccess", "The "
                + pizza.getName()
                + " pizza update with success ");
        return "redirect:/pizza/list";
    }

    @RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
    public String delete(@PathVariable("id") String id, RedirectAttributes redirectAttrs) {
        Pizza pizza = pizzaService.getPizza(id);
        if (!authentication.isAdminUser()) {
            return authentication.getRedirectToLogin();
        }
        pizzaService.deleteData(id);
        redirectAttrs.addFlashAttribute("messageSuccess", "The "
                + pizza.getName()
                + " pizza update with success ");
        return "redirect:/pizza/list";
    }
}
