package com.pizzeria.controller;

import com.pizzeria.authentication.Authentication;
import com.pizzeria.controller.form.OrderFormBean;
import com.pizzeria.domain.Order;
import com.pizzeria.domain.User;
import com.pizzeria.services.OrderService;
import com.pizzeria.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Map;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Autowired
    UserService userService;
    @Autowired
    OrderService orderService;
    @Autowired
    Authentication authentication;

    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String newOrder(ModelMap model,
                           HttpSession session,
                           RedirectAttributes redirectAttrs) {
        Map tempOrder = (Map) session.getAttribute("tempOrder");
        OrderFormBean orderFormBean = new OrderFormBean();
        if (tempOrder == null) {
            redirectAttrs.addFlashAttribute("messageError", "You should put some pizza to checkout");
            return "redirect:/";
        }
        if (tempOrder.containsKey("id")) {
            Order order = orderService.getOrder(tempOrder.get("id").toString());
            orderFormBean.setCompleteDate(order.getDeliveryDate());
        }
        model.addAttribute("orderForm", orderFormBean);
        model.addAttribute("tempOrder", tempOrder);
        return "orderForm";
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public String createOrder(@Valid @ModelAttribute("orderForm") OrderFormBean orderForm,
                              BindingResult result,
                              SessionStatus sessionStatus,
                              HttpSession session,
                              RedirectAttributes redirectAttrs) {
        orderForm.validate(result);
        if (result.hasErrors()) {
            return "orderForm";
        }

        if (!authentication.isLogged()) {
            redirectAttrs.addFlashAttribute("messageError", "You are must be logged to complete the order");
            return authentication.getRedirectToLogin();
        }
        User user = authentication.getUserFromSession();

        Map tempOrder = (Map) session.getAttribute("tempOrder");
        Order newOrder = Order.builder()
                .setDeliveryDate(orderForm.getCompleteDate())
                .setUser(user).build();
        try {

            newOrder = orderService.insertOrUpdateOrderByTempOrderMap(newOrder, tempOrder, user);

        } catch (Exception e) {
            redirectAttrs.addFlashAttribute("messageError", e.getMessage());
            e.printStackTrace();
            return "redirect:/order/show";
        }
        session.setAttribute("tempOrder", null);
        session.removeAttribute("tempOrder");
        sessionStatus.setComplete();

        redirectAttrs.addFlashAttribute("messageSuccess", "The order "
                + newOrder.getId()
                + " created with success ");

        return "redirect:/order/show";
    }

    @RequestMapping(value = "show", method = RequestMethod.GET)
    public String showOwnOrder(ModelMap model, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return "redirect:/login";
        }

        Collection<Order> orders = orderService.getOrderListByUser(user);

        model.addAttribute("orders", orders);
        return "ordersShow";
    }

    @RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
    public String destroyOrder(@PathVariable("id") String orderIdString,
                               RedirectAttributes redirectAttrs) {
        if (!authentication.isLogged()) {
            redirectAttrs.addFlashAttribute("messageError", "You are must be logged to delete the order");
            return authentication.getRedirectToLogin();
        }
        try {

            orderService.deleteData(orderIdString, authentication.getUserFromSession());

            redirectAttrs.addFlashAttribute("messageSuccess", "The order "
                    + orderIdString
                    + " deleted with success ");
        } catch (Exception ex) {
            redirectAttrs.addFlashAttribute("messageError", ex.getMessage());

        }
        return "redirect:/order/show";
    }

    @RequestMapping(value = "{id}/complete", method = RequestMethod.GET)
    public String completeOrder(@PathVariable("id") String orderIdString,
                                RedirectAttributes redirectAttrs) {
        if (!authentication.isLogged()) {
            redirectAttrs.addFlashAttribute("messageError", "You are must be logged to declare complete the order");
            return authentication.getRedirectToLogin();
        }
        try {

            orderService.setComplete(orderIdString, authentication.getUserFromSession());

            redirectAttrs.addFlashAttribute("messageSuccess", "The order "
                    + orderIdString
                    + " declared completed with success ");
        } catch (Exception ex) {
            redirectAttrs.addFlashAttribute("messageError", ex.getMessage());
        }
        return "redirect:/order/show";
    }

    @RequestMapping(value = "{id}/uncompleted", method = RequestMethod.GET)
    public String uncompletedOrder(@PathVariable("id") String orderIdString,
                                   RedirectAttributes redirectAttrs) {
        if (!authentication.isLogged()) {
            redirectAttrs.addFlashAttribute("messageError", "You are must be logged to declare uncompleted the order");
            return authentication.getRedirectToLogin();
        }
        try {

            orderService.setUncompleted(orderIdString, authentication.getUserFromSession());

            redirectAttrs.addFlashAttribute("messageSuccess", "The order "
                    + orderIdString
                    + " declared uncompleted with success ");
        } catch (Exception ex) {
            redirectAttrs.addFlashAttribute("messageError", ex.getMessage());
        }
        return "redirect:/order/show";
    }

    @RequestMapping(value = "{id}/unchangeable", method = RequestMethod.GET)
    public String unchangeableOrder(@PathVariable("id") String orderIdString,
                                    RedirectAttributes redirectAttrs) {
        if (!authentication.isLogged()) {
            redirectAttrs.addFlashAttribute("messageError", "You are must be logged to declare unchangeable the order");
            return authentication.getRedirectToLogin();
        }
        try {

            orderService.setUnchangeable(orderIdString, authentication.getUserFromSession());

            redirectAttrs.addFlashAttribute("messageSuccess", "The order "
                    + orderIdString
                    + " declared unchangeable with success ");
        } catch (Exception ex) {
            redirectAttrs.addFlashAttribute("messageError", ex.getMessage());
        }
        return "redirect:/order/show";
    }

    @RequestMapping(value = "{id}/changeable", method = RequestMethod.GET)
    public String changeableOrder(@PathVariable("id") String orderIdString,
                                  RedirectAttributes redirectAttrs) {
        if (!authentication.isLogged()) {
            redirectAttrs.addFlashAttribute("messageError", "You are must be logged to declare unchangeable the order");
            return authentication.getRedirectToLogin();
        }
        try {

            orderService.setChangeable(orderIdString, authentication.getUserFromSession());

            redirectAttrs.addFlashAttribute("messageSuccess", "The order "
                    + orderIdString
                    + " declared changeable with success ");
        } catch (Exception ex) {
            redirectAttrs.addFlashAttribute("messageError", ex.getMessage());
        }

        return "redirect:/order/show";
    }

    @RequestMapping(value = "{id}/modify", method = RequestMethod.GET)
    public String modifyOrder(@PathVariable("id") String orderIdString,
                              SessionStatus sessionStatus,
                              HttpSession session,
                              RedirectAttributes redirectAttrs) {
        if (!authentication.isLogged()) {
            redirectAttrs.addFlashAttribute("messageError", "You are must be logged to declare complete the order");
            return authentication.getRedirectToLogin();
        }

        Map orderMap = orderService.getOrderMap(orderIdString);

        session.setAttribute("tempOrder", orderMap);
        sessionStatus.setComplete();
        return "redirect:/";
    }
}
