package com.pizzeria.controller;


import com.pizzeria.controller.form.UserLoginFormBean;
import com.pizzeria.controller.form.UserSingUpFormBean;
import com.pizzeria.domain.User;
import com.pizzeria.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "sign-up", method = RequestMethod.GET)
    public String newSignUp(ModelMap model) {
        model.addAttribute("userForm", new UserSingUpFormBean());
        return "userSignUpForm";
    }

    @RequestMapping(value = "sign-up", method = RequestMethod.POST)
    public String create(@ModelAttribute("userForm") @Valid UserSingUpFormBean userForm,
                         BindingResult result,
                         SessionStatus sessionStatus,
                         HttpSession session,
                         RedirectAttributes redirectAttrs) {

        if (result.hasErrors()) {
            return "userSignUpForm";
        }
        User newUser = User.builder()
                .setUsername(userForm.getUsername())
                .setEmail(userForm.getEmail())
                .setPassword(userForm.getPassword()).build();
        User createdUser;
        try {
            createdUser = userService.insertData(newUser, null);
        } catch (Exception ex) {
            result.addError(new FieldError("userForm", "email", ex.getMessage()));
            return "userSignUpForm";
        }
        session.setAttribute("user", createdUser);
        redirectAttrs.addFlashAttribute("messageSuccess", "Welcome into Pizzeria site");
        sessionStatus.setComplete();
        return "redirect:/";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login(ModelMap model) {
        model.put("userForm", new UserLoginFormBean());
        return "userLoginForm";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String login(@ModelAttribute("userForm") @Valid UserLoginFormBean userForm,
                        BindingResult result,
                        SessionStatus sessionStatus,
                        HttpSession session,
                        RedirectAttributes redirectAttrs) {
        User loginUser = userService.login(userForm.getEmailOrUsername(), userForm.getPassword());
        if (loginUser == null) {
            result.addError(new ObjectError("error", "Username or Email or Password are wrong"));
        }
        if (result.hasErrors()) {
            return "userLoginForm";
        }
        session.setAttribute("user", loginUser);
        sessionStatus.setComplete();
        redirectAttrs.addFlashAttribute("messageSuccess", "You are logged");
        return "redirect:/";
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public String logout(SessionStatus sessionStatus,
                         HttpSession session,
                         RedirectAttributes redirectAttrs) {
        session.invalidate();
        redirectAttrs.addFlashAttribute("messageSuccess", "You are log out with success");
        sessionStatus.setComplete();
        return "redirect:/";
    }
}
