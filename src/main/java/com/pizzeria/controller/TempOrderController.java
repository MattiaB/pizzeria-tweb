package com.pizzeria.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/order/temp")
public class TempOrderController {

    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Map> updateTempOrder(@RequestBody Map orderMap,
                                        SessionStatus sessionStatus,
                                        HttpSession session) {

        session.setAttribute("tempOrder", orderMap);
        sessionStatus.setComplete();
        Map<String, String> response = new HashMap<>();
        response.put("response", "ok");
        return new ResponseEntity<Map>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Map> getTempOrder(HttpSession session) {

        Map<String, Object> response = new HashMap<>();
        response.put("order", session.getAttribute("tempOrder"));
        response.put("response", "ok");
        return new ResponseEntity<Map>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Map> deleteTempOrder(SessionStatus sessionStatus,
                                        HttpSession session) {

        Map<String, Object> response = new HashMap<>();
        session.setAttribute("tempOrder", null);
        session.removeAttribute("tempOrder");
        sessionStatus.setComplete();
        response.put("response", "ok");
        return new ResponseEntity<Map>(response, HttpStatus.OK);
    }


}
