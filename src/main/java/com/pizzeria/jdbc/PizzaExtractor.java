package com.pizzeria.jdbc;

import com.pizzeria.domain.Pizza;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PizzaExtractor implements ResultSetExtractor<Pizza> {
    @Override
    public Pizza extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        return Pizza.builder()
                .setId(resultSet.getInt("id"))
                .setName(resultSet.getString("name"))
                .setIngredients(resultSet.getString("ingredients"))
                .setPrice(resultSet.getFloat("price"))
                .setDeleted(resultSet.getBoolean("deleted"))
                .build();
    }
}
