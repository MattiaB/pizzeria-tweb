package com.pizzeria.jdbc;

import com.pizzeria.domain.Order;
import com.pizzeria.domain.Pizza;
import com.pizzeria.domain.SelectedPizza;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SelectedPizzaExtractor implements ResultSetExtractor<SelectedPizza> {
    @Override
    public SelectedPizza extractData(ResultSet resultSet) throws SQLException, DataAccessException {

        Order order = Order.builder()
                .setId(resultSet.getInt("order_id")).build();
        Pizza pizza = Pizza.builder()
                .setId(resultSet.getInt("pizza_id")).build();

        return SelectedPizza.builder()
                .setId(resultSet.getInt("id"))
                .setOrder(order)
                .setPizza(pizza)
                .setQuantity(resultSet.getInt("quantity")).build();
    }
}
