package com.pizzeria.jdbc;

import com.pizzeria.domain.Pizza;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PizzaRowMapper implements RowMapper<Pizza> {
    @Override
    public Pizza mapRow(ResultSet resultSet, int i) throws SQLException {
        PizzaExtractor pizzaExtractor = new PizzaExtractor();
        return pizzaExtractor.extractData(resultSet);
    }
}
