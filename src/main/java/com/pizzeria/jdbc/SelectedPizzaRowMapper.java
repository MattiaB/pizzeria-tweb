package com.pizzeria.jdbc;

import com.pizzeria.domain.SelectedPizza;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SelectedPizzaRowMapper implements RowMapper<SelectedPizza> {
    @Override
    public SelectedPizza mapRow(ResultSet resultSet, int i) throws SQLException {
        SelectedPizzaExtractor selectedPizzaExtractor = new SelectedPizzaExtractor();
        return selectedPizzaExtractor.extractData(resultSet);
    }
}
