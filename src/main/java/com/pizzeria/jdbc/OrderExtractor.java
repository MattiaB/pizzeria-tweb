package com.pizzeria.jdbc;

import com.pizzeria.domain.Order;
import com.pizzeria.domain.Pizza;
import com.pizzeria.domain.SelectedPizza;
import com.pizzeria.domain.User;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class OrderExtractor implements ResultSetExtractor<List<Order>> {
    @Override
    public List<Order> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        Map<Integer, Order.Builder> ordersBuilder = new HashMap<>();

        while (resultSet.next()) {
            Integer orderId = resultSet.getInt("orders_id");
            Order.Builder orderBuilder = ordersBuilder.get(orderId);
            if (orderBuilder == null) {
                orderBuilder = Order.builder()
                        .setId(orderId)
                        .setDeliveryDate(resultSet.getTimestamp("orders_delivery_date"))
                        .setComplete(resultSet.getBoolean("orders_complete"))
                        .setModifiable(resultSet.getBoolean("orders_modifiable"))
                        .setUser(User.builder()
                                .setId(resultSet.getInt("orders_user_id"))
                                .setUsername(resultSet.getString("users_username"))
                                .build())
                        .setSelectedPizzas(new LinkedList<SelectedPizza>());
                ordersBuilder.put(orderId, orderBuilder);
            }
            orderBuilder.getSelectedPizzas().add(
                    SelectedPizza.builder().setId(resultSet.getInt("ord_pizzas_id"))
                            .setQuantity(resultSet.getInt("ord_pizzas_quantity"))
                            .setPizza(Pizza.builder()
                                    .setId(resultSet.getInt("pizzas_id"))
                                    .setName(resultSet.getString("pizzas_name"))
                                    .setIngredients(resultSet.getString("pizzas_ingredients"))
                                    .setPrice(resultSet.getFloat("pizzas_price"))
                                    .build())
                            .build()
            );
        }
        List<Order> orders = new ArrayList<>(ordersBuilder.size());
        for (Order.Builder orderBuilder : ordersBuilder.values()) {
            orders.add(orderBuilder.build());
        }
        return orders;
    }
}
