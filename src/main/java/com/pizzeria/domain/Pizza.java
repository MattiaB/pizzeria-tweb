package com.pizzeria.domain;


public class Pizza {
    final private Number id;
    final private String name;
    final private String ingredients;
    final private Float price;
    final private Boolean deleted;

    public static class Builder {
        private Number id = null;
        private String name = null;
        private String ingredients = null;
        private Float price = null;
        private Boolean deleted = false;

        public Builder setId(Number id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setIngredients(String ingredients) {
            this.ingredients = ingredients;
            return this;
        }

        public Builder setPrice(Float price) {
            this.price = price;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setFromPizza(Pizza pizza) {
            setId(pizza.id);
            setName(pizza.name);
            setIngredients(pizza.ingredients);
            setPrice(pizza.price);
            setDeleted(pizza.deleted);
            return this;
        }

        public Number getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getIngredients() {
            return ingredients;
        }

        public Float getPrice() {
            return price;
        }

        public Boolean getDeleted() {
            return deleted;
        }

        public Pizza build() {
            return new Pizza(id, name, ingredients, price, deleted);
        }

    }

    public static Builder builder() {
        return new Builder();
    }

    public Builder getBuilder() {
        return Pizza.builder().setFromPizza(this);
    }

    private Pizza(Number id, String name, String ingredients, Float price, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.ingredients = ingredients;
        this.price = price;
        this.deleted = deleted;
    }

    public Number getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public Float getPrice() {
        return price;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pizza)) return false;

        Pizza pizza = (Pizza) o;

        if (id != null ? !id.equals(pizza.id) : pizza.id != null) return false;
        if (!ingredients.equals(pizza.ingredients)) return false;
        if (!name.equals(pizza.name)) return false;
        if (!price.equals(pizza.price)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + ingredients.hashCode();
        result = 31 * result + price.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ingredients='" + ingredients + '\'' +
                ", price=" + price +
                '}';
    }
}
