package com.pizzeria.domain;


public class SelectedPizza {
    final private Number id;
    final private Order order;
    final private Number order_id;
    final private Pizza pizza;
    final private Number pizza_id;
    final private Number quantity;

    private SelectedPizza(Number id, Order order, Number order_id, Pizza pizza, Number pizza_id, Number quantity) {
        this.id = id;
        this.order = order;
        this.order_id = order_id;
        this.pizza = pizza;
        this.pizza_id = pizza_id;
        this.quantity = quantity;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Builder getBuilder() {
        return SelectedPizza.builder().setFromSelectedPizza(this);
    }

    public Number getId() {
        return id;
    }

    public Number getOrder_id() {
        return order_id;
    }

    public Number getPizza_id() {
        return pizza_id;
    }

    public Order getOrder() {
        return order;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public Number getQuantity() {
        return quantity;
    }

    public Float getTotalPrice() {
        return pizza.getPrice().floatValue() * quantity.floatValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SelectedPizza)) return false;

        SelectedPizza that = (SelectedPizza) o;

        if (quantity != that.quantity) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!pizza.equals(that.pizza)) return false;
        if (!order.equals(that.order)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + order.hashCode();
        result = 31 * result + pizza.hashCode();
        result = 31 * result + quantity.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SelectedPizza{" +
                "id=" + id +
                ", order=" + order +
                ", pizza=" + pizza +
                ", quantity=" + quantity +
                '}';
    }

    public static class Builder {
        private Number id;
        private Order order;
        private Pizza pizza;
        private Number quantity;

        public Builder setId(Number id) {
            this.id = id;
            return this;
        }

        public Builder setOrder(Order order) {
            this.order = order;
            return this;
        }

        public Builder setPizza(Pizza pizza) {
            this.pizza = pizza;
            return this;
        }

        public Builder setQuantity(Number quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder setFromSelectedPizza(SelectedPizza selectedPizza) {
            setId(selectedPizza.id);
            setOrder(selectedPizza.order);
            setPizza(selectedPizza.pizza);
            setQuantity(selectedPizza.quantity);
            return this;
        }

        public Number getId() {
            return id;
        }

        public Order getOrder() {
            return order;
        }

        public Pizza getPizza() {
            return pizza;
        }

        public Number getQuantity() {
            return quantity;
        }

        public SelectedPizza build() {
            Number orderId = null;
            if (order != null) {
                orderId = order.getId();
            }
            Number pizzaId = null;
            if (pizza != null) {
                pizzaId = pizza.getId();
            }
            return new SelectedPizza(id, order, orderId, pizza, pizzaId, quantity);
        }

    }
}
