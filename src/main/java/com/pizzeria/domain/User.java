package com.pizzeria.domain;

public class User {
    public enum Role {
        NORMAL(0), ADMIN(1);
        private Number code;

        private Role(Number code) {
            this.code = code;
        }

        public Number getCode() {
            return code;
        }
    }

    final private Number id;
    final private String email;
    final private String username;
    final private String password;
    final private Number role;
    final private Role enumRole;

    public static class Builder {
        private Number id;
        private String email;
        private String username;
        private String password;
        private Role role;

        public Builder setId(Number id) {
            this.id = id;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder setRole(Number numberRole) {
            if (numberRole != null) {
                this.role = Role.NORMAL;
                for (Role role : Role.values()) {
                    if (numberRole.equals(role.getCode())) {
                        this.role = role;
                    }
                }
            }
            return this;
        }

        public Builder setRole(Role role) {
            this.role = role;
            return this;
        }

        public Builder setFromUser(User user) {
            setId(user.id);
            setEmail(user.email);
            setUsername(user.username);
            setPassword(user.password);
            setRole(user.role);
            return this;
        }

        public Number getId() {
            return id;
        }

        public String getEmail() {
            return email;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }

        public Role getRole() {
            return role;
        }

        public User build() {
            return new User(id, email, username, password, role);
        }

    }

    public static Builder builder() {
        return new Builder();
    }

    public Builder getBuilder() {
        return User.builder().setFromUser(this);
    }

    private User(Number id, String email, String username, String password, Role role) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.password = password;
        if (role == null) {
            this.role = null;
        } else {
            this.role = role.getCode();
        }
        this.enumRole = role;

    }

    public Number getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Number getRole() {
        return role;
    }

    public Role getEnumRole() {
        return enumRole;
    }

    public boolean is(Role comparedRole) {
        return role.equals(comparedRole.getCode());
    }

    public boolean isAdmin() {
        return is(Role.ADMIN);
    }

    public boolean isNormal() {
        return is(Role.NORMAL);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (!email.equals(user.email)) return false;
        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (!role.equals(user.role)) return false;
        if (!username.equals(user.username)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + email.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + role.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
