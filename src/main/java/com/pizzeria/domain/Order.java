package com.pizzeria.domain;

import java.util.Collection;
import java.util.Date;

public class Order {
    final private Number id;
    final private User user;
    final private Number userId;
    final private Collection<SelectedPizza> selectedPizzas;
    final private Date deliveryDate;
    final private Boolean modifiable;
    final private Boolean complete;

    public static class Builder {
        private Number id;
        private User user;
        private Collection<SelectedPizza> selectedPizzas;
        private Date deliveryDate;
        private Boolean modifiable = true;
        private Boolean complete = false;

        public Builder setId(Number id) {
            this.id = id;
            return this;
        }

        public Builder setUser(User user) {
            this.user = user;
            return this;
        }

        public Builder setSelectedPizzas(Collection<SelectedPizza> selectedPizza) {
            this.selectedPizzas = selectedPizza;
            return this;
        }

        public Builder setDeliveryDate(Date deliveryDate) {
            this.deliveryDate = deliveryDate;
            return this;
        }

        public Builder setFromOrder(Order order) {
            if (order != null) {
                setId(order.id);
                setUser(order.user);
                setSelectedPizzas(order.selectedPizzas);
                setDeliveryDate(order.deliveryDate);
                setComplete(order.complete);
                setModifiable(order.modifiable);
            }
            return this;
        }

        public Builder setModifiable(Boolean modifiable) {
            this.modifiable = modifiable;
            return this;
        }

        public Builder setComplete(Boolean complete) {
            this.complete = complete;
            return this;
        }

        public Number getId() {
            return id;
        }

        public User getUser() {
            return user;
        }

        public Collection<SelectedPizza> getSelectedPizzas() {
            return selectedPizzas;
        }

        public Date getDeliveryDate() {
            return deliveryDate;
        }

        public Boolean getModifiable() {
            return modifiable;
        }

        public Boolean getComplete() {
            return complete;
        }

        public Order build() {
            Number userId = null;
            if (user != null) {
                userId = user.getId();
            }
            return new Order(id, user, userId, selectedPizzas, deliveryDate, modifiable, complete);
        }

    }

    public static Builder builder() {
        return new Builder();
    }

    public Builder getBuilder() {
        return Order.builder().setFromOrder(this);
    }

    private Order(Number id, User user, Number userId,
                  Collection<SelectedPizza> pizzas, Date deliveryDate, Boolean modifiable, Boolean complete) {
        this.id = id;
        this.user = user;
        this.userId = userId;
        this.selectedPizzas = pizzas;
        this.deliveryDate = deliveryDate;
        this.modifiable = modifiable;
        this.complete = complete;
    }

    public Number getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Number getUserId() {
        return userId;
    }

    public Collection<SelectedPizza> getSelectedPizzas() {
        return selectedPizzas;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public Float getTotalPrice() {
        float total = 0;
        for (SelectedPizza selectedPizza : selectedPizzas) {
            total += selectedPizza.getTotalPrice();
        }
        return total;
    }

    public Integer getTotalQuantity() {
        int total = 0;
        for (SelectedPizza selectedPizza : selectedPizzas) {
            total += selectedPizza.getQuantity().intValue();
        }
        return total;
    }

    public Boolean getModifiable() {
        return modifiable;
    }

    public Boolean getComplete() {
        return complete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;

        Order order = (Order) o;

        if (deliveryDate != null ? !deliveryDate.equals(order.deliveryDate) : order.deliveryDate != null) return false;
        if (id != null ? !id.equals(order.id) : order.id != null) return false;
        if (selectedPizzas != null ? !selectedPizzas.equals(order.selectedPizzas) : order.selectedPizzas != null)
            return false;
        if (user != null ? !user.equals(order.user) : order.user != null) return false;
        if (userId != null ? !userId.equals(order.userId) : order.userId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + user.hashCode();
        result = 31 * result + (selectedPizzas != null ? selectedPizzas.hashCode() : 0);
        result = 31 * result + deliveryDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "deliveryDate='" + deliveryDate + '\'' +
                ", user=" + user +
                ", id=" + id +
                '}';
    }
}
