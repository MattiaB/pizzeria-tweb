INSERT INTO pizzas (id, name, ingredients, price, deleted)
VALUES (0, 'Margherita', 'Pomodoro, mozzarella', 3.50, FALSE);
INSERT INTO pizzas (id, name, ingredients, price, deleted) VALUES (1, 'Bianca', 'mozzarella, origano', 3.00, FALSE);
INSERT INTO users (id, email, username, password, role) VALUES (0, 'test@test.com', 'test', 'password', 0);
INSERT INTO users (id, email, username, password, role) VALUES (1, 'test1@test.com', 'test1', 'password', 1);
INSERT INTO orders (id, user_id, delivery_date, modifiable, complete) VALUES (0, 0,'2011-05-16 15:36:38', 1, 0);
INSERT INTO orders (id, user_id, delivery_date, modifiable, complete) VALUES (1, 1, '2011-05-16 15:36:38', 1, 0);
INSERT INTO order_pizzas (id, pizza_id, order_id, quantity) VALUES (0, 0, 0, 3);
INSERT INTO order_pizzas (id, pizza_id, order_id, quantity) VALUES (1, 1, 0, 2);
INSERT INTO order_pizzas (id, pizza_id, order_id, quantity) VALUES (2, 1, 1, 1);

