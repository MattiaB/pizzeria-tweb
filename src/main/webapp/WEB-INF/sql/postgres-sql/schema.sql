CREATE SCHEMA IF NOT EXISTS PUBLIC;

CREATE SEQUENCE user_id_seq;

CREATE SEQUENCE order_id_seq;

CREATE SEQUENCE pizza_id_seq;

CREATE SEQUENCE order_pizza_id_seq;

CREATE TABLE IF NOT EXISTS public.users
(
  id       INT PRIMARY KEY DEFAULT NEXTVAL('user_id_seq') NOT NULL,
  email    VARCHAR                                        NOT NULL,
  password VARCHAR                                        NOT NULL,
  role     INT                                            NOT NULL,
  username VARCHAR                                        NOT NULL
);
CREATE TABLE IF NOT EXISTS public.pizzas
(
  id          INT PRIMARY KEY DEFAULT NEXTVAL('pizza_id_seq') NOT NULL,
  name        VARCHAR                                         NOT NULL,
  ingredients VARCHAR                                         NOT NULL,
  price       FLOAT                                           NOT NULL,
  deleted     BOOLEAN                                         NOT NULL
);

CREATE TABLE IF NOT EXISTS public.order_pizzas
(
  id       INT PRIMARY KEY DEFAULT NEXTVAL('order_pizza_id_seq') NOT NULL,
  pizza_id INT                                                   NOT NULL,
  order_id INT                                                   NOT NULL,
  quantity INT                                                   NOT NULL
);

CREATE TABLE IF NOT EXISTS public.orders
(
  id            INT PRIMARY KEY DEFAULT NEXTVAL('order_id_seq')       NOT NULL,
  user_id       INT                                                   NOT NULL,
  delivery_date DATE                                                  NOT NULL,
  modifiable    BOOLEAN                                               NOT NULL,
  complete      BOOLEAN                                               NOT NULL

);
INSERT INTO public.pizzas (name, ingredients, price, deleted)
  SELECT
    'Margherita',
    'Pomodoro, Mozzarella, Origano',
    4.20,
    FALSE
  WHERE
    NOT EXISTS(
        SELECT
          name
        FROM public.pizzas
        WHERE name = 'Margherita'
    );
INSERT INTO public.pizzas (name, ingredients, price, deleted)
  SELECT
    'Greca',
    'Pomodoro, Mozzarella, Olive Nere',
    5.20,
    FALSE
  WHERE
    NOT EXISTS(
        SELECT
          name
        FROM public.pizzas
        WHERE name = 'Greca'
    );
INSERT INTO public.pizzas (name, ingredients, price, deleted)
  SELECT
    'Girasole',
    'Pomodoro, Mozzarella, Scamorza, Rucola',
    5.90,
    FALSE
  WHERE
    NOT EXISTS(
        SELECT
          name
        FROM public.pizzas
        WHERE name = 'Girasole'
    );
INSERT INTO public.pizzas (name, ingredients, price, deleted)
  SELECT
    '4 Stagioni',
    'Pomodoro, Mozzarella, Prosciutto cotto, Funghi, Carciofi, Olive Nere',
    6.30,
    FALSE
  WHERE
    NOT EXISTS(
        SELECT
          name
        FROM public.pizzas
        WHERE name = '4 Stagioni'
    );
INSERT INTO public.pizzas (name, ingredients, price, deleted)
  SELECT
    'Verdure',
    'Pomodoro, Mozzarella, Verdure miste fresce',
    6.0,
    FALSE
  WHERE
    NOT EXISTS(
        SELECT
          name
        FROM public.pizzas
        WHERE name = 'Verdure'
    );
INSERT INTO public.users (email, username, password, role)
  SELECT
    'admin@admin.it',
    'admin',
    'admin',
    1
  WHERE
    NOT EXISTS(
        SELECT
          username
        FROM public.users
        WHERE username = 'admin'
    );