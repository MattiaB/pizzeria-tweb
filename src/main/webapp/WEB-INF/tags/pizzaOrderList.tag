<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script id="pizza-order-template" type="text/x-dot-template">
    {{~it :value}}
    <li id="pizza-order-id-{{=value.id}}">
        <div class="col-xs-6 col-md-6">
            {{=value.name}}
        </div>
        <div class="col-xs-2 col-md-2">
            {{=value.total_price.toFixed(2)}}&#8364;
        </div>
        <div class="col-xs-4 col-md-4">
            <div class="btn-group">
                <button type="button" data-pizzaid="{{=value.id}}" class="btn-xs btn-default btn-minus-pizza">
                    <span class="glyphicon glyphicon-minus"></span>
                </button>
                {{=value.quantity}}
                <button type="button" data-pizzaid="{{=value.id}}" class="btn-xs btn-default btn-plus-pizza">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
            </div>
        </div>
    </li>
    {{~}}
</script>
<script id="pizza-list-template" type="text/x-dot-template">
    {{~it :value}}
    <tr>
        <td>
                    <span class="pizza-name block">
                        {{=value.name}}
                    </span>
                    <span class="pizza-ingredients block">
                        {{=value.ingredients}}
                    </span>
        </td>
        <td>
                    <span class="pizza-price">
                        {{=value.price}}&#8364;
                    </span>
        </td>
        <td>
            <button data-pizzaid="{{=value.id}}" class="btn add-to-order">Add to Order</button>
        </td>
    </tr>
    {{~}}
</script>
<div class="row">
    <div class="col-md-8">
        <table id="pizzas-table" class="table table-condensed">
            <thead>
            <tr>
                <th>Name <br/>
                    Ingredients
                </th>
                <th>Price</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div id="basket" class="col-md-4 list-group">
        <h3>
            Basket
        </h3>

        <div class="row">
            <div class="col-xs-6 col-md-6">
                Pizza
            </div>
            <div class="col-xs-2 col-md-2">
                Total price
            </div>
            <div class="col-xs-4 col-md-4 text-center">
                #
            </div>
        </div>
        <div class="row">
            <ul id="order-pizza-list" class="list-unstyled">

            </ul>
        </div>
        <div class="row total-order">
            <div class="col-xs-6 col-md-6">
                Total
            </div>
            <div id="total-price-order" class=" col-xs-2 col-md-2">

            </div>
            <div id="total-quantity-order" class="col-xs-4 col-md-4 text-center">

            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <a type="button" class="btn btn-primary" href="${pageContext.request.contextPath}/order/new">Complete
                    Order</a>
                <button id="deleteTempOrder" type="button" class="btn btn-danger">Delete Order</button>
            </div>
        </div>
    </div>
</div>
