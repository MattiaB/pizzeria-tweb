<%--@elvariable id="user" type="com.pizzeria.domain.User"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="${pageContext.request.contextPath}/">Pizzeria</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="${pageContext.request.contextPath}/#pizzas-table">Book some Pizzas</a></li>
        <c:if test="${user ne null}">
            <li><a href="${pageContext.request.contextPath}/order/show">List Orders</a></li>
        </c:if>
    </ul>

    <form class="navbar-form navbar-left hidden" role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <c:if test="${user eq null}">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="${pageContext.request.contextPath}/login">Sing-in</a></li>
        </ul>
    </c:if>
    <c:if test="${user ne null}">
    <div class="navbar-right">
        <c:if test="${user.isAdmin()}">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <c:out value="${user.username}"/>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${pageContext.request.contextPath}/order/show">List Orders</a></li>
                        <li><a href="${pageContext.request.contextPath}/pizza/new">Add Pizza</a></li>
                        <li><a href="${pageContext.request.contextPath}/pizza/list">Manage Pizza</a></li>
                    </ul>
                </li>
            </ul>
            </c:if>
            <ul class="nav navbar-nav">
                <c:if test="${user.isNormal()}">
                    <li><a href="${pageContext.request.contextPath}/order/show">
                        <c:out value="${user.username}"/>
                    </a></li>
                </c:if>
                <li><a href="${pageContext.request.contextPath}/logout">
                    Sign out
                </a></li>
            </ul>
            </c:if>
        </div>
    </div>
    <!-- /.navbar-collapse -->
</nav>