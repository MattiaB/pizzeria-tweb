<%@ tag description="Main Layout" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@attribute name="javascript" fragment="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Pizzeria</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<c:url value="/resources/css/normalize.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/css/main.css" />"
          rel="stylesheet" type="text/css"/>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<c:url value="/resources/js/lib/html5.js" />"></script>
    <![endif]-->
    <script data-main="<c:url value="/resources/js/main.js" />"
            src="<c:url value="/resources/js/lib/require.js" />"></script>
    <jsp:invoke fragment="javascript"/>

</head>

<body>
<t:navbar/>
<c:if test="${messageSuccess ne null}">
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <c:out value="${messageSuccess}"/></div>
</c:if>
<c:if test="${messageError ne null}">
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <c:out value="${messageError}"/></div>
</c:if>
<div class="container">

    <jsp:doBody/>

    <t:footer/>

</div>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-40798199-2', 'pizzeria-tw.herokuapp.com');
    ga('send', 'pageview');

</script>
</body>
</html>