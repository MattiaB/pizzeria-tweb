<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:main-layout>
    <jsp:attribute name="javascript">
          <script src="<c:url value="/resources/js/app/orderApp.js" />"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="jumbotron">
            <div class="main-pizza-img-div">
                <img async class="img-responsive"
                     src="https://dl.dropboxusercontent.com/u/14672643/Supreme_pizza_small.jpg">
            </div>

            <p><a class="btn btn-primary btn-lg go-to-pizza-table" role="button">Let's order some pizzas</a></p>
        </div>
        <t:pizzaOrderList/>
    </jsp:body>
</t:main-layout>
