<%--@elvariable id="user" type="com.pizzeria.domain.User"--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<t:main-layout>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Id</th>
            <th>Delivery Date Time</th>
            <th>
                <div class="row">
                    <div class="col-md-6">
                        Pizza name
                    </div>
                    <div class="col-md-2">
                        Quantity
                    </div>
                    <div class="col-md-4 text-center">
                        Total price
                    </div>
                </div>
            </th>
            <th>Status</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="order" items="${orders}">
            <tr>
                <td>
                    <c:out value="${order.id}"/>
                </td>
                <td>
                    <fmt:formatDate value="${order.deliveryDate}" type="both"
                                    pattern="dd/MM/yyyy - HH:mm"/>
                </td>
                <td>
                    <c:forEach var="pizza" items="${order.selectedPizzas}">
                        <div class="row">
                            <div class="col-md-6">
                                <c:out value="${pizza.pizza.name}"/>
                            </div>
                            <div class="col-md-2 text-center">
                                <c:out value="${pizza.quantity}"/>
                            </div>
                            <div class="col-md-4 text-center">
                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
                                                  value="${pizza.totalPrice}"/>&#8364;
                            </div>
                        </div>
                    </c:forEach>
                </td>
                <td>
                    <p class="block">Total:
                        <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
                                          value="${order.totalPrice}"/>&#8364;
                    </p>

                    <p class="block">
                        <c:if test="${order.complete}">
                            Complete
                        </c:if>
                        <c:if test="${order.modifiable}">
                            Modifiable
                        </c:if>
                        <c:if test="${!order.modifiable}">
                            Unmodifiable
                        </c:if>
                    </p>
                    <c:if test="${user.isAdmin()}">
                        <p class="block">
                            Username: <c:out value="${order.user.username}"/>
                        </p>
                    </c:if>
                </td>
                <td>
                    <c:if test="${!order.complete}">
                        <a href="${pageContext.request.contextPath}/order/<c:out value="${order.id}"/>/complete"
                           class="btn btn-primary">Delivery is Complete</a>
                    </c:if>
                    <c:if test="${order.modifiable}">
                        <a href="${pageContext.request.contextPath}/order/<c:out value="${order.id}"/>/modify"
                           class="btn">Modify</a>

                        <form class="inline"
                              action="${pageContext.request.contextPath}/order/<c:out value="${order.id}"/>/delete"
                              method="POST">
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </c:if>
                    <c:if test="${user.isAdmin()}">
                        <c:if test="${order.modifiable}">
                            <a href="${pageContext.request.contextPath}/order/<c:out value="${order.id}"/>/unchangeable"
                               class="btn">Set Unchangeable</a>
                        </c:if>
                        <c:if test="${!order.modifiable}">
                            <a href="${pageContext.request.contextPath}/order/<c:out value="${order.id}"/>/changeable"
                               class="btn">Set Changeable</a>
                        </c:if>
                        <c:if test="${order.complete}">
                            <a href="${pageContext.request.contextPath}/order/<c:out value="${order.id}"/>/uncompleted"
                               class="btn">Set Uncompleted</a>
                        </c:if>
                    </c:if>
                </td>


            </tr>
        </c:forEach>
        </tbody>
    </table>
</t:main-layout>