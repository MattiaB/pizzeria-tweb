<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<t:main-layout>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form:form id="form" action="${pageContext.request.contextPath}/pizza/${actionType}" method="post"
                       modelAttribute="pizzaForm" class="form-horizontal" role="form">
                <s:bind path="*">
                    <c:if test="${status.error}">
                        <div id="message" class="alert alert-danger">
                            <c:forEach items="${status.errorMessages}" var="error">
                                <c:out value="${error}"/><br/>
                            </c:forEach>
                        </div>
                    </c:if>
                </s:bind>
                <legend>Create a pizza</legend>

                <div class="form-group error">
                    <form:label class="col-lg-2 control-label" path="name">
                        Name
                    </form:label>
                    <div class="col-lg-6">
                        <form:input path="name" class="form-control" placeholder="Choose a pizza name"
                                    required="required"/>
                        <s:bind path="name">
                            <c:if test="${status.error}">
                                <div class="alert alert-danger">
                                    <form:errors path="name" cssClass="error"/>
                                </div>
                            </c:if>
                        </s:bind>
                    </div>
                </div>
                <div class="form-group warning">
                    <form:label class="col-lg-2 control-label" path="ingredients">
                        Ingredients
                    </form:label>
                    <div class="col-lg-6">
                        <form:input class="form-control" path="ingredients" placeholder="Ingredients"/>
                        <s:bind path="ingredients">
                            <c:if test="${status.error}">
                                <div class="alert alert-danger">
                                    <form:errors path="ingredients" cssClass="error"/>
                                </div>
                            </c:if>
                        </s:bind>
                    </div>
                </div>
                <div class="form-group success">
                    <form:label class="col-lg-2 control-label" path="price">
                        Price
                    </form:label>
                    <div class="col-lg-6">
                        <form:input type="text" pattern="^\d*(\.\d{1,2}$)?" size="4" class="form-control" path="price"
                                    placeholder="Price" required="required"/>
                        <s:bind path="price">
                            <c:if test="${status.error}">
                                <div class="alert alert-danger">
                                    <form:errors path="price" cssClass="error"/>
                                </div>
                            </c:if>
                        </s:bind>
                    </div>
                </div>
                <p>
                    <button type="submit" class="btn btn-default">Submit</button>
                </p>
            </form:form>
        </div>
    </div>
</t:main-layout>