<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<t:main-layout>
    <form:form id="form" action="${pageContext.request.contextPath}/order/create" method="post"
               modelAttribute="orderForm" class="form-horizontal" role="form">
        <legend>Complete the order</legend>
        <div class="form-group ">
            <form:label class="col-sm-2 control-label" path="date">
                Date
            </form:label>
            <div class="col-sm-10">
                <form:input path="date" type="text" class="form-control"
                            placeholder="Choose a date in this format dd/MM/yyyy ex. 20/10/2013"
                            required="required"/>
                <s:bind path="date">
                    <c:if test="${status.error}">
                        <div class="alert alert-danger">
                            <form:errors path="date"/>
                        </div>
                    </c:if>
                </s:bind>
            </div>
        </div>
        <div class="form-group ">
            <form:label class="col-sm-2 control-label" path="time">
                Time
            </form:label>
            <div class="col-sm-10">
                <form:input path="time" type="text" class="form-control"
                            placeholder="Choose a time in this format hh:mm ex. 13:45"
                            required="required"/>
                <s:bind path="time">
                    <c:if test="${status.error}">
                        <div class="alert alert-danger">
                            <form:errors path="time"/>
                        </div>
                    </c:if>
                </s:bind>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-2 control-label">
                Pizzas
            </label>

            <div class="col-sm-10">
                <div class="list-group">
                    <c:forEach var="pizza" items="${tempOrder.selectedPizzas}">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading"><c:out value="${pizza.name}"/></h4>

                            <div class="list-group-item-text">
                                <p><c:out value="${pizza.ingredients}"/></p>

                                <p class="text-right">
                                    <span class="block">Quantità <c:out value="${pizza.quantity}"/></span>
                                    <span class="block">Prezzo totale
                                        <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
                                                          value="${pizza.total_price}"/>&#8364;</span>
                                </p>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-2 control-label">
                Total Price
            </label>

            <div class="col-sm-10">
                <p class="form-control-static">
                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
                                      value="${tempOrder.total_price}"/>&#8364;
                </p>
            </div>
            <label class="col-sm-2 control-label">
                Total quantity
            </label>

            <div class="col-sm-10">
                <p class="form-control-static">
                    <c:out value="${tempOrder.total_quantity}"/></p>
            </div>
        </div>
        <p>
            <button type="submit" class="btn btn-primary">Checkout</button>
            <a href="${pageContext.request.contextPath}/" class="btn">Modify Order</a>
        </p>
    </form:form>

</t:main-layout>

