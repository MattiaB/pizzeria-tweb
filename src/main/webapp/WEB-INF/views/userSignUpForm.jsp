<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<t:main-layout>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form:form id="form" method="post" modelAttribute="userForm" class="form-horizontal" role="form">
                <s:bind path="*">
                    <c:if test="${status.error}">
                        <div id="message" class="alert alert-danger">
                            <c:forEach items="${status.errorMessages}" var="error">
                                <c:out value="${error}"/><br/>
                            </c:forEach>
                        </div>
                    </c:if>
                </s:bind>
                <legend>Sign Up</legend>

                <div class="form-group error">
                    <form:label class="col-sm-2 control-label" path="username">
                        Choose your Username
                    </form:label>
                    <div class="col-sm-10">
                        <form:input path="username" class="form-control" placeholder="Choose your Username"
                                    required="required"/>
                        <s:bind path="username">
                            <c:if test="${status.error}">
                                <div class="alert alert-danger">
                                    <form:errors path="username" cssClass="error"/>
                                </div>
                            </c:if>
                        </s:bind>
                    </div>
                </div>
                <div class="form-group warning">
                    <form:label class="col-sm-2 control-label" path="email">
                        Email
                    </form:label>
                    <div class="col-sm-10">
                        <form:input type="email" class="form-control" path="email" placeholder="Email"
                                    required="required"/>
                        <s:bind path="email">
                            <c:if test="${status.error}">
                                <div class="alert alert-danger">
                                    <form:errors path="email"/>
                                </div>
                            </c:if>
                        </s:bind>
                    </div>
                </div>
                <div class="form-group success">
                    <form:label class="col-sm-2 control-label" path="password">
                        Create a Password
                    </form:label>
                    <div class="col-sm-10">
                        <form:input type="password" class="form-control" path="password" placeholder="Create a Password"
                                    required="required"/>
                        <s:bind path="password">
                            <c:if test="${status.error}">
                                <div class="alert alert-danger">
                                    <form:errors path="password" cssClass="error"/>
                                </div>
                            </c:if>
                        </s:bind>
                    </div>
                </div>
                <div class="form-group">
                    <form:label class="col-sm-2 control-label" path="confirmPassword">
                        Confirm your Password
                    </form:label>
                    <div class="col-sm-10">
                        <form:input type="password" class="form-control" path="confirmPassword"
                                    placeholder="Confirm your Password" required="required"/>
                        <s:bind path="confirmPassword">
                            <c:if test="${status.error}">
                                <div class="alert alert-danger">
                                    <form:errors path="confirmPassword" cssClass="error"/>
                                </div>
                            </c:if>
                        </s:bind>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Sign in</button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</t:main-layout>
