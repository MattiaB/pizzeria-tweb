<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<t:main-layout>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form:form class="form-horizontal" method="post" modelAttribute="userForm" role="form">
                <s:bind path="*">
                    <c:if test="${status.error}">
                        <div class="alert alert-danger">
                            <c:forEach var="error" items="${status.errorMessages}">
                                <c:out value="${error}"/>
                                </br>
                            </c:forEach>
                        </div>
                    </c:if>
                </s:bind>
                <legend>Login</legend>

                <div class="form-group">
                    <form:label class="col-sm-2 control-label" path="emailOrUsername">
                        Email or Username <form:errors path="emailOrUsername"/>
                    </form:label>
                    <div class="col-sm-10">
                        <form:input path="emailOrUsername" class="form-control" placeholder="Your Email or Username"
                                    required="required"/>
                    </div>
                </div>
                <div class="form-group">
                    <form:label class="col-sm-2 control-label" path="password">
                        Password <form:errors path="password"/>
                    </form:label>
                    <div class="col-sm-10">
                        <form:input type="password" class="form-control" path="password" placeholder="******"
                                    required="required"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"/> Remember me
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Sign in</button>
                    </div>
                </div>
            </form:form>
            <p class="text-center">
                <a href="${pageContext.request.contextPath}/sign-up" class="">Create an account </a>
            </p>
        </div>
    </div>
</t:main-layout>
