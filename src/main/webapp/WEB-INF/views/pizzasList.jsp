<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<t:main-layout>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Name</th>
            <th>Ingredients</th>
            <th>Price</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="pizza" items="${pizzas}">
            <tr>
                <td><c:out value="${pizza.name}"/></td>
                <td>
                    <c:out value="${pizza.ingredients}"/>

                </td>
                <td>
                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2"
                                      value="${pizza.price}"/>

                </td>
                <td>
                    <a href="${pageContext.request.contextPath}/pizza/<c:out value="${pizza.id}"/>/edit"
                       class="btn btn-warning">Modify</a>

                    <form action="${pageContext.request.contextPath}/pizza/${pizza.id}/delete" method="post"
                          class="inline" role="form">
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>


            </tr>
        </c:forEach>
        </tbody>
    </table>

</t:main-layout>
