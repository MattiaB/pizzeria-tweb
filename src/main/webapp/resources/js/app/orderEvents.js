/*global define:false, $:false, document:false*/

define(
    ['app/orderView', 'jquery', 'app/orderHttp'],
    function (orderView, $, orderHttp) {
        "use strict";
        var htmlAllBasket = $('#basket'), handleWhenBtnMinusClick, handleWhenBtnPlusClick, updateOrder;
        handleWhenBtnPlusClick = function () {
            var pizzaId = $(this).attr('data-pizzaid');
            orderView.addPizzaToOrder(pizzaId);
            orderHttp.updateOrder();
        };
        handleWhenBtnMinusClick = function () {
            var pizzaId = $(this).attr('data-pizzaid');
            orderView.removePizzaToOrder(pizzaId);
            orderHttp.updateOrder();
        };
        orderView.calledWhenIsUpdate(function () {
            $('.btn-minus-pizza').click(handleWhenBtnMinusClick);
            $('.btn-plus-pizza').click(handleWhenBtnPlusClick);
        });

        updateOrder = function () {

        };

        return {
            initialize: function () {
                if (document.body.clientWidth > 992) {
                    htmlAllBasket.affix({
                        offset: {
                            top: htmlAllBasket.position().top
                        }
                    });
                }
                $.fn.scrollView = function () {
                    return this.each(function () {
                        $('html, body').animate({
                            scrollTop: $(this).offset().top
                        }, 500);
                    });
                };
                $('.go-to-pizza-table').click(function () {
                    $('#pizzas-table').scrollView();
                });

                orderHttp.getPizzas(function () {
                    orderView.showPizzas();
                    orderHttp.getOrder(function () {
                        orderView.update();
                        $('.add-to-order').click(function () {
                            var pizzaId = $(this).attr('data-pizzaid');
                            orderView.addPizzaToOrder(pizzaId);
                            orderHttp.updateOrder();
                        });
                    });

                });
                $('#deleteTempOrder').click(function () {
                    orderHttp.deleteOrder(function () {
                        orderHttp.getOrder(function () {
                            orderView.update();
                        });

                    });
                });


            }
        };
    }
);