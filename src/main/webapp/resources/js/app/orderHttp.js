/*global define:false, $:false, console:false*/

define(
    ['app/orderModel', 'jquery'],
    function (orderModel) {
        "use strict";
        var callbackPizzas, callbackOrder;
        callbackPizzas = $.Callbacks();

        $.ajax({
            dataType: "json",
            url: 'pizza/list.json',
            success: function (data) {
                orderModel.setPizzas(data);
                callbackPizzas.fire(orderModel);
            }
        });

        return {
            getPizzas: function (callback) {
                if (orderModel.getPizzas() === undefined) {
                    callbackPizzas.add(callback);
                } else {
                    callback(orderModel);
                }
            },
            updateOrder: function () {
                $.ajax({
                    dataType: "json",
                    contentType: 'application/json',
                    data: JSON.stringify(orderModel.getOrder()),
                    url: 'order/temp',
                    type: 'POST',
                    success: function (data) {
                        console.log(data);
                    }
                });
            },
            getOrder: function (callback) {
                $.ajax({
                    dataType: "json",
                    url: 'order/temp',
                    success: function (data) {
                        orderModel.setOrder(data.order);
                        callback(orderModel);
                    }
                });
            },
            deleteOrder: function (callback) {
                $.ajax({
                    dataType: "json",
                    contentType: 'application/json',
                    url: 'order/temp/delete',
                    type: 'POST',
                    success: function (data) {
                        console.log(data);
                        callback(data);
                    }
                });
            }
        };
    }
);