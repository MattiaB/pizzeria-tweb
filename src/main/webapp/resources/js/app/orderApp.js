/*global require:false */
require(['app/orderEvents'],
    function (orderEvents) {
        "use strict";
        orderEvents.initialize();

    });