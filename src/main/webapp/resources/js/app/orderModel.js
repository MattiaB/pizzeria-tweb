/*global define:false, _:false */

define(
    ['underscore'],
    function () {
        "use strict";
        var pizzas, order, findById, initOrder;
        findById = function (arrayData, id) {
            id = parseInt(id, 10);
            return _.first(_.filter(arrayData, function (elem) {
                return elem.id === id;
            }));
        };
        initOrder = function () {
            var newOrder = {selectedPizzas: []};
            newOrder.total_price = 0.0;
            newOrder.total_quantity = 0;
            return newOrder;
        };
        order = initOrder();
        return {
            setPizzas: function (data) {
                pizzas = data;
            },
            getPizzas: function () {
                return pizzas;
            },
            getOrder: function () {
                return order;
            },
            setOrder: function (data) {
                if (data === null) {
                    data = initOrder();
                }
                order = data;
            },
            getSelectedPizzas: function () {
                return order.selectedPizzas;
            },
            addPizzaToOrder: function (pizzaId) {
                var pizza, pizzaOrder;
                pizzaId = parseInt(pizzaId, 10);
                pizzaOrder = findById(order.selectedPizzas, pizzaId);
                if (pizzaOrder === undefined) {
                    pizza = findById(pizzas, pizzaId);
                    pizzaOrder = {
                        id: pizzaId,
                        name: pizza.name,
                        ingredients: pizza.ingredients,
                        pizza_price: pizza.price,
                        total_price: 0.0,

                        quantity: 0
                    };
                    order.selectedPizzas.push(pizzaOrder);
                }
                pizzaOrder.total_price += pizzaOrder.pizza_price;
                pizzaOrder.quantity += 1;
                order.total_price += pizzaOrder.pizza_price;
                order.total_quantity += 1;

                return pizzaOrder;
            },
            removePizzaToOrder: function (pizzaId) {
                var findedSelectedPizza = findById(order.selectedPizzas, pizzaId);
                if (findedSelectedPizza !== undefined) {
                    findedSelectedPizza.total_price -= findedSelectedPizza.pizza_price;
                    findedSelectedPizza.quantity -= 1;
                    order.total_price -= findedSelectedPizza.pizza_price;
                    order.total_quantity -= 1;
                    if (findedSelectedPizza.quantity === 0) {
                        order.selectedPizzas = _.reject(order.selectedPizzas, function (elem) {
                            return elem.id === findedSelectedPizza.id;
                        });
                    }
                }
                return findedSelectedPizza;
            }
        };
    }
);