/*global define:false, document:false*/

define(
    ['app/orderModel', 'jquery', 'doT', 'bootstrap'],
    function (orderModel, $, doT) {
        "use strict";
        var renderOrdersList, renderPizzaList, pizzaTable, orderList, callbackview, orderTotalPrice, orderTotalQuantity;
        renderOrdersList = doT.template(document.getElementById("pizza-order-template").text);
        renderPizzaList = doT.template(document.getElementById("pizza-list-template").text);
        pizzaTable = $('#pizzas-table').find('tbody').first();
        orderList = $('#order-pizza-list');
        orderTotalPrice = $('#total-price-order');
        orderTotalQuantity = $('#total-quantity-order');
        callbackview = $.Callbacks();


        return {
            showPizzas: function () {
                pizzaTable.html(renderPizzaList(orderModel.getPizzas()));
            },
            addPizzaToOrder: function (pizzaId) {
                orderModel.addPizzaToOrder(pizzaId);
                this.update();
            },
            removePizzaToOrder: function (orderId) {
                orderModel.removePizzaToOrder(orderId);
                this.update();
            },
            update: function () {
                orderList.html(renderOrdersList(orderModel.getSelectedPizzas()));
                orderTotalPrice.html(orderModel.getOrder().total_price.toFixed(2).toString() + '&#8364;');
                orderTotalQuantity.html(orderModel.getOrder().total_quantity);
                callbackview.fire();
            },
            calledWhenIsUpdate: function (handle) {
                callbackview.add(handle);
                handle();
            }
        };
    }
);