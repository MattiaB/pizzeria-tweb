/*global requirejs:false, require:false, _:false, $:false, console:false */


requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: 'resources/js/lib',
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: {
        app: '../app',
        jquery: ['http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min',
            'jquery'],
        underscore: 'underscore',
        doT: 'doT',
        bootstrap: ['http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/js/bootstrap.min',
            'bootstrap']
    },
    shim: {
        "bootstrap": {
            deps: ["jquery"]
        },
        'underscore': {
            exports: '_'
        }
    }
});
requirejs.onError = function (err) {
    "use strict";
    console.log(err.requireType);
    if (err.requireType === 'timeout') {
        console.log('modules: ' + err.requireModules);
    }

    throw err;
};
require(['jquery', 'bootstrap']);
