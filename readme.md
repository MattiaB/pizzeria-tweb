#Pizzeria
##Web Technology course project

This project is useful to understand the basic of web developing in Java with Spring.

###Server side tools
- Java
- JSP
- Spring
- Junit
- Postgres
- Maven
- Git

###Client side tools
- Javascript
    - RequireJS
    - jQuery
    - Underscore
    - doT
- Css
    - Bootstrap
- HTML5

###Other tools
- IntelliJ IDEA (IDE)
- Heroku (Cloud Hosting)  [Pizzeria](http://pizzeria-tw.herokuapp.com/ "Pizzeria")